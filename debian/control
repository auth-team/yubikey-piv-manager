Source: yubikey-piv-manager
Maintainer: Debian Authentication Maintainers <pkg-auth-maintainers@lists.alioth.debian.org>
Uploaders: Dag Heyman <dag@yubico.com>,
	   Dain Nilsson <dain@yubico.com>,
           Klas Lindfors <klas@yubico.com>
Section: utils
Priority: optional
Build-Depends: debhelper (>= 11),
               dh-exec,
               python (>= 2.7),
               python-nose,
               python-setuptools (>= 0.6b3)
Standards-Version: 4.2.1
Homepage: https://developers.yubico.com/yubikey-piv-manager/
Vcs-Browser: https://salsa.debian.org/auth-team/yubikey-piv-manager
Vcs-Git: https://salsa.debian.org/auth-team/yubikey-piv-manager.git

Package: yubikey-piv-manager
Architecture: all
Depends: yubico-piv-tool (>= 1.4.3),
         python-pyside.qtgui,
         python-pyside.qtnetwork,
         python-pkg-resources,
         ${misc:Depends},
         ${python:Depends}
Recommends: pcscd
Description: Graphical tool for managing your PIV-enabled YubiKey
 This tool is used for performing administrative tasks on a PIV-enabled
 YubiKey. It allows management of PIN, PUK, as well as the Management Key,
 and can be used to generate and/or import keys and certificates.
 This is a GUI that wraps yubico-piv-tool, and provides some additional
 functionality.
